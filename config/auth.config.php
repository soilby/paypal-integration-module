<?php

return array(
    'bjyauthorize' => [
        'guards' => [
            'BjyAuthorize\Guard\Route' => [
                // Resource
                ['route' => 'talaka_paypal_payment/prepare', 'roles' => ['user']],
                ['route' => 'talaka_paypal_payment/capture', 'roles' => ['user']],
                ['route' => 'talaka_paypal_payment/done', 'roles' => ['user']],
                ['route' => 'talaka_paypal_payment/notify', 'roles' => ['user']],
                ['route' => 'talaka_paypal_payment/start', 'roles' => ['user']],


            ]
        ]
    ]
);