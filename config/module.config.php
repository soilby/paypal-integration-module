<?php

namespace Incubator;

return array(
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    ), // doctrine

    'paypal_integration' => [
        'username' => '',
        'password' => '',
        'signature' => '',
        'merchantId' => '',
        'sandbox_endpoint' => 'https://www.sandbox.paypal.com/cgi-bin/webscr',
        'production_endpoint' => 'https://www.paypal.com/cgi-bin/webscr',
        'sandbox' => true,
        'storeName' => 'Test Shop',
        'currencyCode' => 'EUR',
        'returnURL' => '/payment/callback/success/paypal',
        'cancelReturnUrl' => '/payment/callback/fail/paypal',
        'notifyUrl' => 'http://testshop/notify'

    ]
);