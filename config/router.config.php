<?php

return [
    'router' => [
        'routes' => [
            'talaka_paypal_payment' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/paypal',
                    'defaults' => [
                        'controller' => 'TalakaPaypalPaymentController',
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'prepare' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/prepare',
                            'defaults' => [
                                'action' => 'prepare',
                            ],
                        ],
                    ],
                    'capture' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/capture',
                            'defaults' => [
                                'action' => 'capture',
                            ],
                        ],
                    ],
                    'done' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/done',
                            'defaults' => [
                                'action' => 'done',
                            ],
                        ],
                    ],
                    'notify' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/notify',
                            'defaults' => [
                                'action' => 'notify',
                            ],
                        ],
                    ],
                    'start' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/start',
                            'defaults' => [
                                'action' => 'index',
                            ],
                        ],
                    ]
                ]
            ]
        ]
    ]
];
