<?php

return array(
    'service_manager' => array(
        'alias' => [

        ],
        'invokables' => [
        ],
        'factories' => [
//            'TalakaPayPalPayment' => 'Talaka\PayPalIntegration\Service\Factory\PaymentFactory',
            'TalakaPayPalTransactionBuilder' => 'Talaka\PayPalIntegration\Service\Factory\TransactionBuilderFactory',
        ]
    ),
    'controllers' => array(
        'invokables' => [
//            'TalakaPaypalPaymentController' => 'Talaka\PayPalIntegration\Controller\PaymentController'
        ],
        'factories' => [
//            'TalakaPaypalPaymentController' => 'Talaka\PayPalIntegration\Controller\Factory\PaymentControllerFactory'
        ]
    ),
    'view_helpers' => array(
        'invokables' => array(
        ),
        'factories' => [
            'payPalIntegrationWidget' => 'Talaka\PayPalIntegration\View\Factory\WidgetFactory'
        ]
    )
);