<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 21.5.15
 * Time: 19.35
 */
namespace Talaka\PayPalIntegration\Controller\Factory;

use Talaka\PayPalIntegration\Controller\PaymentController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PaymentControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $paypalPayment = $serviceLocator->getServiceLocator()->get('TalakaPayPalPayment');
        $controller = new PaymentController($paypalPayment);

        return $controller;
    }


}