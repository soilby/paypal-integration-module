<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 21.5.15
 * Time: 19.26
 */

namespace Talaka\PayPalIntegration\Controller;

use Payum\Core\Reply\HttpRedirect;
use Payum\Core\Request\Capture;
use Payum\Core\Request\GetHumanStatus;
use Payum\Core\Request\Notify;
use Talaka\PayPalIntegration\Entity\Notification;
use Talaka\PayPalIntegration\Entity\PaymentDetails;
use Talaka\PayPalIntegration\Service\Payment;
use Talaka\PayPalIntegration\Service\TransactionBuilder;
use Zend\Mvc\Controller\AbstractActionController;

class PaymentController2 extends AbstractActionController {

    /**
     * @var Payment
     */
    protected $paypalPayment;

    public function __construct($paypalPayment)    {
        $this->paypalPayment = $paypalPayment;

    }


    public function prepareAction()    {
        $gatewayName = 'paypal_express_checkout';

        $payum = $this->paypalPayment->getPayum();

        $storage = $payum->getStorage($this->paypalPayment->getOrderClass());

        $order = $storage->create();
        $order->setNumber(uniqid());
        $order->setCurrencyCode('EUR');
        $order->setTotalAmount(100); // 1.23 EUR
        $order->setDescription('A description');
        $order->setClientId('anId');
        $order->setClientEmail('grgnvk@gmail.com');

//        $order->setDetails(array(
//            'L_PAYMENTREQUEST_0_DESC0' => 'A desc',
//        ));


        $storage->update($order);

        $tokenFactory = $this->paypalPayment->getTokenFactory();
        $captureToken = $tokenFactory->createCaptureToken($gatewayName, $order, $this->paypalPayment->getPath('done'));



        $storage = $payum->getStorage('Talaka\PayPalIntegration\Entity\PaymentDetails');


        $paymentDetails = new PaymentDetails();
        $storage->update($paymentDetails);

        $notifyToken = $tokenFactory->createNotifyToken($gatewayName, $paymentDetails);

        $paymentDetails['NOTIFYURL'] = $notifyToken->getTargetUrl();

        return $this->redirect()->toUrl($captureToken->getTargetUrl());
    }

    public function captureAction() {
        $requestVerifier = $this->paypalPayment->getRequestVerifier();
        $token = $requestVerifier->verify($_REQUEST);
        $gateway = $this->paypalPayment->getPayum()->getGateway($token->getGatewayName());

        if ($reply = $gateway->execute(new Capture($token), true)) {
            if ($reply instanceof HttpRedirect) {
                header("Location: ".$reply->getUrl());
                die();
            }

            throw new \LogicException('Unsupported reply', null, $reply);
        }

        $requestVerifier->invalidate($token);

        header("Location: ".$token->getAfterUrl());

        exit('CAPTURE');

    }


    public function doneAction()    {
        $requestVerifier = $this->paypalPayment->getRequestVerifier();
        $payum = $this->paypalPayment->getPayum();

        $token = $requestVerifier->verify($_REQUEST);

        $gateway = $payum->getGateway($token->getGatewayName());

        // you can invalidate the token. The url could not be requested any more.
        // $requestVerifier->invalidate($token);

        // Once you have token you can get the model from the storage directly.
        //$identity = $token->getDetails();
        //$order = $payum->getStorage($identity->getClass())->find($identity);

        // or Payum can fetch the model for you while executing a request (Preferred).
        $gateway->execute($status = new GetHumanStatus($token));
        $order = $status->getFirstModel();

        header('Content-Type: application/json');
        echo json_encode([
            'status' => $status->getValue(),
            'order' => [
                'total_amount' => $order->getTotalAmount(),
                'currency_code' => $order->getCurrencyCode(),
                'details' => $order->getDetails(),
            ],
        ]);

        exit();
    }

    public function notifyAction()  {
        $data = $this->getRequest()->getContent();
        file_put_contents('/tmp/notification', $data . PHP_EOL, FILE_APPEND);


        $requestVerifier = $this->paypalPayment->getRequestVerifier();
        $payum = $this->paypalPayment->getPayum();

        $token = $requestVerifier->verify();
        $gateway = $payum->getGateway($token->getGatewayName());

        $ret = $gateway->execute(new Notify($token));
        var_dump($token);
        var_dump($ret);
        exit();
    }

} 