<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 20.4.15
 * Time: 4.04
 */

namespace Talaka\PayPalIntegration\Service\Factory;


use Payum\Paypal\ExpressCheckout\Nvp\PaypalExpressCheckoutGatewayFactory;
use Talaka\PayPalIntegration\Service\TransactionBuilder;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Talaka\PayPalIntegration\Service\Payment;

class PaymentFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        if (!array_key_exists('paypal_integration', $config))  {
            throw new \Exception('PayPal Integration module should be configure under paypal_integration key');
        }

        $moduleConfig = $config['paypal_integration'];

        $factory = new PaypalExpressCheckoutGatewayFactory();

        $gateway = $factory->create(array(
            'username' => $moduleConfig['username'],
            'password' => $moduleConfig['password'],
            'signature' => $moduleConfig['signature'],
            'sandbox' => true,
        ));

        $router = $serviceLocator->get('router');

        $service = new Payment($gateway);
        $service->setPath('capture', $router->assemble([], ['name' => 'talaka_paypal_payment/capture']));
        $service->setPath('done', $router->assemble([], ['name' => 'talaka_paypal_payment/done']));
        $service->setPath('notify', 'http://dev.talaka.by' . $router->assemble([], ['name' => 'talaka_paypal_payment/notify']));
//        $service->setPath('notify', $router->assemble([], ['name' => 'talaka_paypal_payment/notify'], ['force_canonical' => true]));

        $service->configure();

        return $service;
    }
}