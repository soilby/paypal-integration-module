<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 20.4.15
 * Time: 4.04
 */

namespace Talaka\PayPalIntegration\Service\Factory;


use Talaka\PayPalIntegration\Service\TransactionBuilder;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TransactionBuilderFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $config = $serviceLocator->get('config');
        if (!array_key_exists('paypal_integration', $config))  {
            throw new \Exception('PayPal Integration module should be configure under paypal_integration key');
        }

        $moduleConfig = $config['paypal_integration'];

        $sandbox = array_key_exists('sandbox', $moduleConfig) ? $moduleConfig['sandbox'] : false;

        if ($sandbox)   {
            if (!array_key_exists('sandbox_endpoint', $moduleConfig))  {
                throw new \Exception("Sandbox endpoint didn't set. Please specify under paypal_integration/sandbox_endpoint key.");
            }
            $endpointURL = $moduleConfig['sandbox_endpoint'];
        }
        else    {
            if (!array_key_exists('production_endpoint', $moduleConfig))  {
                throw new \Exception("Production endpoint didn't set. Please specify under paypal_integration/production_endpoint key.");
            }
            $endpointURL = $moduleConfig['production_endpoint'];
        }

        $service = new TransactionBuilder($endpointURL);

        return $service;
    }
}