<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 26.5.15
 * Time: 22.29
 */

namespace Talaka\PayPalIntegration\Service;


use Buzz\Client\ClientInterface;
use Buzz\Client\Curl;
use Buzz\Message\Form\FormRequest;
use Buzz\Message\Response;

class IpnApi {

    /**
     * It sends back if the message originated with PayPal.
     */
    const NOTIFY_VERIFIED = 'VERIFIED';

    /**
     * if there is any discrepancy with what was originally sent
     */
    const NOTIFY_INVALID = 'INVALID';

    const CMD_NOTIFY_VALIDATE = '_notify-validate';

    /**
     * @var \Buzz\Client\ClientInterface
     */
    protected $client;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @param string $endpoint
     * @param ClientInterface $client
     */
    public function __construct($endpoint, ClientInterface $client = null)
    {
        $this->client = $client ?: $this->createCurl();

        $this->endpoint = $endpoint;

    }

    public function createCurl()
    {
        $client = new Curl();

        //reaction to the ssl3.0 shutdown from paypal
        //https://www.paypal-community.com/t5/PayPal-Forward/PayPal-Response-to-SSL-3-0-Vulnerability-aka-POODLE/ba-p/891829
        //http://googleonlinesecurity.blogspot.com/2014/10/this-poodle-bites-exploiting-ssl-30.html
        $client->setOption(CURLOPT_SSL_CIPHER_LIST, 'TLSv1');

        //There is a constant for that CURL_SSLVERSION_TLSv1, but it is not present on some versions of php.
        $client->setOption(CURLOPT_SSLVERSION, $curlSslVersionTlsV1 = 1);

        return $client;
    }




    /**
     * @param array $notification
     *
     * @return string
     */
    public function notifyValidate(array $notification)
    {
        $request = new FormRequest();
        $request->setField('cmd', self::CMD_NOTIFY_VALIDATE);
        $request->addFields($notification);
        $request->setMethod('POST');
        $request->fromUrl($this->endpoint);
        $request->addHeader('User-Agent: anuseragent');

        $response = new Response();

        $this->client->send($request, $response);

        if (false == $response->isSuccessful()) {
            $status = $response->getStatusCode();

            throw new \Exception("IPN Validation Request fail with $status");
        }

        if (self::NOTIFY_VERIFIED === $response->getContent()) {
            return self::NOTIFY_VERIFIED;
        }

        return self::NOTIFY_INVALID;
    }
} 