<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 21.5.15
 * Time: 21.35
 */

namespace Talaka\PayPalIntegration\Service;


use Payum\Core\Bridge\PlainPhp\Security\TokenFactory;
use Payum\Core\Bridge\PlainPhp\Security\HttpRequestVerifier;
use Payum\Core\Registry\SimpleRegistry;
use Payum\Core\Storage\FilesystemStorage;
use Payum\Core\Security\GenericTokenFactory;

use Payum\Core\Gateway;
use Payum\Paypal\ExpressCheckout\Nvp\PaypalExpressCheckoutGatewayFactory;
use Talaka\PayPalIntegration\Action\StoreNotificationAction;


class Payment {

    protected $pathConfig = [];

    protected $payum;
    protected $tokenFactory;
    protected $requestVerifier;

    protected $orderClass = 'Payum\Core\Model\Payment';

    public function configure() {
        $notificationStorage = new FilesystemStorage('/tmp/payum_notification', 'Talaka\PayPalIntegration\Entity\Notification', 'id');
        $paymentDetailsStorage = new FilesystemStorage('/tmp/payum_details', 'Talaka\PayPalIntegration\Entity\PaymentDetails', 'id');
        $storages = [
            $this->orderClass => new FilesystemStorage('/tmp/payum_storages', $this->orderClass, 'number'),
            'Talaka\PayPalIntegration\Entity\Notification' => $notificationStorage,
            'Talaka\PayPalIntegration\Entity\PaymentDetails' => $paymentDetailsStorage
        ];

        $gateways = ['paypal_express_checkout' => $this->gateway];

        $this->payum = new SimpleRegistry($gateways, $storages);


        $tokenStorage = new FilesystemStorage('/tmp/payum_token', 'Payum\Core\Model\Token', 'hash');

        $this->requestVerifier = new HttpRequestVerifier($tokenStorage);

        $this->tokenFactory = new GenericTokenFactory(
            new TokenFactory($tokenStorage, $this->payum),
            $this->pathConfig
        );

        /*IPN*/
        $storeNotificationAction = new StoreNotificationAction(
            $notificationStorage
        );

        $this->payum->getGateway('paypal_express_checkout')->addAction($storeNotificationAction);


    }

    public function __construct(Gateway $gateway)   {
        $this->gateway = $gateway;

    }

    /**
     * @return SimpleRegistry
     */
    public function getPayum()
    {
        return $this->payum;
    }

    /**
     * @return string
     */
    public function getOrderClass()
    {
        return $this->orderClass;
    }

    /**
     * @return GenericTokenFactory
     */
    public function getTokenFactory()
    {
        return $this->tokenFactory;
    }

    public function setPath($action, $path) {
        $this->pathConfig[$action] = $path;
    }

    public function getPath($action)    {
        return array_key_exists($action, $this->pathConfig) ? $this->pathConfig[$action] : null;
    }

    /**
     * @return mixed
     */
    public function getRequestVerifier()
    {
        return $this->requestVerifier;
    }


} 