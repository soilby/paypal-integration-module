<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 17.4.15
 * Time: 2.32
 */

namespace Talaka\PayPalIntegration\Service;



use Payum\Core\Bridge\Buzz\ClientFactory;
use Payum\Paypal\Ipn\Api;
use Talaka\Payment\Entity\Notification;
use Talaka\Payment\Entity\OrderAbstract;
use Talaka\Payment\Entity\Transaction;
use Talaka\Payment\Service\OrderService;
use Talaka\Payment\Transaction\BuilderInterface;
use Zend\Http\Request;

class TransactionBuilder implements BuilderInterface {

    /**
     * @see https://developer.paypal.com/docs/classic/ipn/integration-guide/IPNandPDTVariables/
     */
    const TRANSACTION_TYPE_COMPLETED = 'Completed';
    const TRANSACTION_TYPE_PROCESSED = 'Processed';
    const TRANSACTION_TYPE_PENDING = 'Pending';

    const TRANSACTION_TYPE_REFUNDED = 'Refunded';
    const TRANSACTION_TYPE_REVERSED= 'Reversed';
    const TRANSACTION_TYPE_VOIDED = 'Voided';

    const TRANSACTION_TYPE_DENIED = 'Denied';
    const TRANSACTION_TYPE_EXPIRED = 'Expired';
    const TRANSACTION_TYPE_FAILED = 'Failed';
    const TRANSACTION_TYPE_CANCELED_REVERSAL = 'Canceled_Reversal';


    /**
     * @var
     */
    protected $gateway;

    protected $endpoint;

    public function __construct($endpoint)   {
        $this->endpoint = $endpoint;
    }


    /**
     * @param Request $request
     *
     * @return Notification
     * @throws \Exception
     */
    public function handleNotification(Request $request)    {
        $content = $request->getContent();

        $body = urldecode($content);
        $fields = [];
        parse_str($body, $fields);

        $api = new IpnApi($this->endpoint);

        $status = $api->notifyValidate($fields);
        if ($status !== IpnApi::NOTIFY_VERIFIED) {
            throw new \Exception("Notification not accepted by paypal");
        }

        return $this->parseNotification($fields);
    }


    public function parseNotification($fields) {

        $notification = new Notification();

        $notification->currency = $fields['mc_currency']; //currency of payment
        $notification->amount = $fields['mc_gross'];
        $notification->foreignOrderId = $fields['txn_id'];
        $notification->orderId = $fields['custom'];
        $notification->foreignTransactionId = $fields['txn_id'];

        $time = strtotime($fields['payment_date']);
        $timeString = date('c', $time);
        $notification->date = new \DateTime($timeString);
        $notification->transactionType = $fields['payment_status'];

        foreach ($fields as &$field)    {
            $field = mb_convert_encoding($field, "UTF-8", "ISO-8859-1");
        }

//        $fields['item_name'] = 'ITEMNAME';
        $notification->origin = $fields;


        return $notification;

    }

    public function updateTransaction(Transaction $transaction, Notification $parsedFields = null)  {
        if (!is_null($parsedFields->transactionType)) {

            switch ($parsedFields->transactionType) {

                case self::TRANSACTION_TYPE_COMPLETED:
                    $status = OrderAbstract::STATUS_CONFIRMED;
                    break;

                case self::TRANSACTION_TYPE_PROCESSED:
                case self::TRANSACTION_TYPE_PENDING:
                    $status = OrderAbstract::STATUS_PLACED;
                    break;

                case self::TRANSACTION_TYPE_REFUNDED:
                    $status = OrderAbstract::STATUS_REFUNDED;
                    break;

                default:
                    $status = OrderAbstract::STATUS_CANCELED;
                    break;
            }

            $transaction->setStatus($status);


        }
        else    {
            $order = $transaction->getOrder();
            $transaction->setAmount($order->getPrice());
        }



        return $transaction;
    }

    /**
     * @param Request $request
     *
     * @return Notification
     */
    public function getTransaction(Request $request)    {
        $orderId = $request->getQuery('orderId');

        $notification = new Notification();

        $notification->orderId = $orderId;

        return $notification;
    }

}