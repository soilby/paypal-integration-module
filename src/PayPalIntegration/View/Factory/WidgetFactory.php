<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 27.3.15
 * Time: 21.57
 */

namespace Talaka\PayPalIntegration\View\Factory;


use Talaka\PayPalIntegration\View\Widget;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class WidgetFactory implements FactoryInterface {

    protected $serverUrl;

    public function createService(ServiceLocatorInterface $serviceLocator)  {

        $this->serverUrl = $serviceLocator->get('ServerUrl')->__invoke();

        $config = $serviceLocator->getServiceLocator()->get('config');
        if (!array_key_exists('paypal_integration', $config))  {
            throw new \Exception('PayPal Integration module should be configure under paypal_integration key');
        }

        $moduleConfig = $config['paypal_integration'];

        $sandbox = array_key_exists('sandbox', $moduleConfig) ? $moduleConfig['sandbox'] : false;

        if ($sandbox)   {
            if (!array_key_exists('sandbox_endpoint', $moduleConfig))  {
                throw new \Exception("Sandbox endpoint didn't set. Please specify under paypal_integration/sandbox_endpoint key.");
            }
            $endpointURL = $moduleConfig['sandbox_endpoint'];
        }
        else    {
            if (!array_key_exists('production_endpoint', $moduleConfig))  {
                throw new \Exception("Production endpoint didn't set. Please specify under paypal_integration/production_endpoint key.");
            }
            $endpointURL = $moduleConfig['production_endpoint'];
        }

        if (!array_key_exists('merchantId', $moduleConfig))    {
            throw new \Exception("PayPal merchantId didn't set");
        }
        $merchantId = $moduleConfig['merchantId'];

        if (!array_key_exists('storeName', $moduleConfig))    {
            throw new \Exception("Store Name didn't set");
        }
        $storeName = $moduleConfig['storeName'];

        if (!array_key_exists('currencyCode', $moduleConfig))    {
            throw new \Exception("Currency code didn't set");
        }
        $currencyCode = $moduleConfig['currencyCode'];

        if (!array_key_exists('returnURL', $moduleConfig))    {
            throw new \Exception("returnURL didn't set");
        }
        $returnURL = $this->normalizeLocalURL($moduleConfig['returnURL']);

        if (!array_key_exists('cancelReturnUrl', $moduleConfig))    {
            throw new \Exception("cancelReturnUrl didn't set");
        }
        $cancelReturnUrl = $this->normalizeLocalURL($moduleConfig['cancelReturnUrl']);

        $widget = new Widget($endpointURL, $merchantId, $storeName, $currencyCode, $returnURL, $cancelReturnUrl);

        if (array_key_exists('secret_key', $moduleConfig))    {
            $secretKey = $moduleConfig['secret_key'];

            $widget->setSecretKey($secretKey);
        }



        if (array_key_exists('notifyUrl', $moduleConfig))    {
            $notifyUrl = $this->normalizeLocalURL($moduleConfig['notifyUrl']);
            $widget->setNotifyURL($notifyUrl);
        }

        return $widget;
    }

    protected function normalizeLocalURL($url)  {
        if (strpos($url, 'http') !== 0) {
            return $this->serverUrl . $url;
        }

        return $url;
    }

}