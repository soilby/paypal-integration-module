<?php
namespace Talaka\PayPalIntegration\View;

use User\Entity\User;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 27.3.15
 * Time: 20.48
 */
class Widget extends AbstractHelper {

    protected $endpointUrl;
    protected $merchantId;
    protected $storeName;
    protected $currencyCode;

    protected $purpose; //item_name

    protected $returnUrl;
    protected $cancelReturnUrl;
    protected $notifyUrl;


    protected $total = 0;

    protected $orderId;

    /**
     * @var User
     */
    protected $customer;

    public function __construct($endpointURL, $merchantId, $storeName,
                                $currencyCode, $returnURL, $cancelReturnURL)    {
        $this->endpointUrl = $endpointURL;
        $this->merchantId = $merchantId;
        $this->storeName = $storeName;
        $this->currencyCode = $currencyCode;
        $this->returnUrl = $returnURL;
        $this->cancelReturnUrl = $cancelReturnURL;
    }

    public function setCustomer($customer)  {
        $this->customer = $customer;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }





    public function __invoke($total, $orderId, $title, $currencyCode = null)  {
        $this->total = $total;
        $this->orderId = $orderId;
        $this->purpose = $title;

        $this->returnUrl .= '?orderId=' . $orderId;
        $this->cancelReturnUrl .= '?orderId=' . $orderId;

        if ($currencyCode)  {
            $this->currencyCode = $currencyCode;
        }

        return $this;
    }

    public function __toString()    {

        $viewModel = new ViewModel([
            'endpointUrl' => $this->endpointUrl,
            'merchantId' => $this->merchantId,
            'storeName' => $this->storeName,
            'currencyCode' => $this->currencyCode,
            'returnUrl' => $this->returnUrl,
            'notifyUrl' => $this->notifyUrl,
            'cancelReturnUrl' => $this->cancelReturnUrl,

            'currencyId' => $this->currencyCode,
            'total' => $this->total,

            'purpose' => $this->purpose,

            'orderId' => $this->orderId
        ]);

        if ($this->customer)    {
            $viewModel->setVariable('email', $this->customer->getEmail());
            $viewModel->setVariable('phone', $this->customer->getPhone());
        }

        $viewModel->setTemplate('paypal-integration-module/widget');

        $content = $this->view->render($viewModel);

        return $content;
    }

    /**
     * @param mixed $notifyUrl
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
    }


}